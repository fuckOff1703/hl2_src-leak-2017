# hl2_src
Leaked 2017-2018 source engine code.

## Development Setup

### Install Valve Steam Runtime
```
sudo mkdir /valve
sudo chown your-username: /valve
cd /valve
wget http://media.steampowered.com/client/runtime/steam-runtime-sdk_latest.tar.xz
tar xvf steam-runtime-sdk_latest.tar.xz
rm steam-runtime-sdk_latest.tar.xz
mv steam-runtime-sdk_* steam-runtime
cd steam-runtime
```

```
$ ./setup.sh
======================================
Which architectures would you like to target?
    1) i386 (x86 32-bit)
    2) amd64 (x64 64-bit)
    3) all supported architectures
Default i386: 1
Set target architecture to: i386

======================================
Which runtime flavor would you like to use?
    1) release
    2) debug
Default release: 1
Set runtime flavor to: release

======================================
Update base SDK? [Y/n]: Y
Installing http://media.steampowered.com/client/runtime/steam-runtime-sdk_latest.tar.xz...
##################################################################################################################################### 100.0%

======================================
Update tools? [Y/n]: Y
Installing http://media.steampowered.com/client/runtime/x-tools-amd64-i386_latest.tar.xz...
##################################################################################################################################### 100.0%

======================================
Update runtime? [Y/n]: Y
Installing http://media.steampowered.com/client/runtime/steam-runtime-dev-release_latest.tar.xz...
##################################################################################################################################### 100.0%

======================================
Update complete!
```

Ensure that /bin is part of PATH by patching shell.sh:
```
sed -i 's#export PATH="${TOP}/bin:$PATH"#export PATH="${TOP}/bin:$PATH:/bin"#' /valve/steam-runtime/shell.sh
```

### Enable Valve Steam Runtime
**Must be enabled each time you work with the source engine.**

`/valve/steam-runtime/shell.sh --arch=i386`

---

##### Build devtools/vpc_linux

Optional (already done in this repo)

```
cd external/vpc/utils/vpc
make -j
```

## Configuring

cstrike on linux:
```
devtools/bin/vpc /f /linux32 /define:NO_AUTO_P4 +everything -physics -bugreporter_filequeue -itemtest_com -itemtest_lib -sdktoolslib -vaudio_miles -video_bink -video_quicktime -video_webm -simdtest -gcsdk -panel_zoo -stdshader_dbg /cstrike /mksln cstrike
```

cstrike dedicated on linux:
```
devtools/bin/vpc_linux /f /linux32 /define:NO_AUTO_P4 +dedicated -physics -gcsdk /cstrike /dedicated /mksln cstrike_dedicated
```

## Building

cstrike:
`make -f cstrike.mak`

cstrike_dedicated:
`make -f cstrike_dedicated.mak`

## Game content

### Download from Steam Depot

[SteamDB 240](https://steamdb.info/app/240/depots/)

[SteamRE/DepotDownloader](https://github.com/SteamRE/DepotDownloader)

Grab the latest release.
Install mono and run:

`mono DepotDownloader.dll -app 240 -username xxx -password xxx`

